package edu.ntnu.idatt2001.bjornjob.oblig3.cardgame.cardclasses;

import java.util.*;

/**
 * @author bjornjob
 * @version 1.o
 * @since 2020-03-25
 */
public class DeckOfCards {
    private final char[] suit = {'S','H','D','C'};
    private ArrayList<PlayingCard> deck = new ArrayList<>();

    /**
     * Constructor for the class {@link DeckOfCards}. This constructor creates a deck with 52 unique cards when an
     * object of the class is initiated.
     */
    public DeckOfCards() {
        for (char c : suit) {
            for(int i = 1; i < 14; i++) {
                deck.add(new PlayingCard(c,i));
            }
        }
    }

    /**
     * A method which picks a given number of cards randomly from the deck.
     * @param numberOfCards is the requested number of random cards from the deck.
     * @return an ArrayList consisting of the requested number of cards.
     * @throws IllegalArgumentException if inputted number of cards is less than 1 og greater than 52.
     */
    public ArrayList<PlayingCard> dealHand(int numberOfCards) throws IllegalArgumentException {
        checkNumberInputForInvalidInput(numberOfCards);
        shuffleDeck();

        ArrayList<PlayingCard> playingCards = new ArrayList<>();
        for (int i = 0; i < numberOfCards; i++) {
            playingCards.add(deck.get(i));
        }

        return playingCards;
    }

    /**
     * A method which throws an IllegalArgumentException if inputted number of cards is less than 1 og greater than 52.
     * @param numberOfCards is the user inputted request for number of cards on the hand.
     */
    private void checkNumberInputForInvalidInput(int numberOfCards) throws IllegalArgumentException {
        if (numberOfCards < 1 || numberOfCards > 52) {
            throw new IllegalArgumentException("Inputted number must be between 1 and 52");
        }
    }

    /**
     * A method which utilizes Collections method shuffle to randomize the cards of the deck.
     */
    public void shuffleDeck() {
        Collections.shuffle(deck);
    }

    /**
     * Accessor method which returns the deck of cards.
     * @return an ArrayList of the deck.
     */
    public ArrayList<PlayingCard> getDeck() {
        return deck;
    }

    /**
     * Get string version of a PlayingCard object.
     * @return a string representing an object of the class {@link DeckOfCards}.
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Deck of cards:\n");
        for(PlayingCard card : deck) {
            sb.append(card.toString()).append(" ");
        }
        return sb.toString();
    }

}