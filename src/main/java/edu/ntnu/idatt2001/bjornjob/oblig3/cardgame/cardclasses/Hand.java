package edu.ntnu.idatt2001.bjornjob.oblig3.cardgame.cardclasses;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * @author bjornjob
 * @version 1.o
 * @since 2020-03-25
 */
public class Hand {
    private final ArrayList<PlayingCard> playingCards;

    /**
     * Constructor for a hand of cards.
     * @param playingCards is the cards on the players hand.
     */
    public Hand(ArrayList<PlayingCard> playingCards) {
        this.playingCards = playingCards;
    }

    /**
     * A method which calculates the sum of faces of the hand.
     * @return an integer representing the sum of faces.
     */
    public int getSumOfFaces() {
        return playingCards.stream()
                .mapToInt(PlayingCard::getFace)
                .sum();
    }

    /**
     * A method which finds all the cards of hearts which is on the hand.
     * @return a string representing the cards of hearts on the hand. If there are no cards of hearts on the hand, the
     * method return a string 'No hearts'.
     */
    public String getAllHeartsOnHand() {
        ArrayList<PlayingCard> cardsOfHearts = playingCards.stream()
                .filter(card -> card.getSuit() == 'H')
                .collect(Collectors.toCollection(ArrayList::new));

        if(cardsOfHearts.isEmpty())
            return "No hearts";

        return cardsOfHearts.stream()
                .map(PlayingCard::getAsString)
                .collect(Collectors.joining(", "));

        //Alternative methods
        /**
         List<String> cardsOfHearts = playingCards.stream()
         .filter(playingCard -> playingCard.getSuit() == 'H')
         .map(PlayingCard::getAsString)
         .toList();

         if(cardsOfHearts.isEmpty())
         return "No hearts";

         StringBuilder sb = new StringBuilder();
         cardsOfHearts.forEach(playingCard -> sb.append(playingCard + " "));
         return String.valueOf(sb);
         */
    }

    /**
     * A method which checks if the queen of spades is on the hand.
     * @return true if the queen of spades is present on the hand, false if not.
     */
    public boolean queenOfSpadesPresent () {
        return playingCards.stream()
                .anyMatch(playingCard -> playingCard.getAsString().equals("S12"));

        //Alternative methods
        /**
         return playingCards.stream()
         .anyMatch(playingCard -> playingCard.getSuit() == 'S' && playingCard.getFace() == 12);

         return playingCards.stream()
         .filter(playingCard -> playingCard.getSuit() == 'S' && playingCard.getFace() == 12)
         .count() == 1;
         */
    }

    /**
     * A method which checks if there is a flush on the hand.
     * @return true if the hand contains a flush, false if not.
     */
    public boolean handContainsFlush() {
        return playingCards.stream()
                .map(PlayingCard::getSuit)
                .distinct()
                .count() == 1;
    }

    /**
     * Get string version of a Hand object.
     * @return a string representing an object of the class {@link Hand}.
     */
    @Override
    public String toString() {
        return playingCards.stream()
                .map(PlayingCard::getAsString)
                .collect(Collectors.joining(", "));
    }
}