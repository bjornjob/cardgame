package edu.ntnu.idatt2001.bjornjob.oblig3.cardgame;

import edu.ntnu.idatt2001.bjornjob.oblig3.cardgame.cardclasses.*;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class Controller {
    private Hand hand;

    @FXML
    private Label cardsOnHand;
    @FXML
    private Label sumOfFacesLabel;
    @FXML
    private Label allHeartsOnHandLabel;
    @FXML
    private Label queenOfSpadesPresentLabel;
    @FXML
    private Label handContainsFlushLabel;

    @FXML
    protected void dealHand() {
        DeckOfCards deck = new DeckOfCards();
        hand = new Hand(deck.dealHand(5));

        cardsOnHand.setText(hand.toString());
    }

    @FXML
    protected void checkHand() {
        sumOfFacesLabel.setText(String.valueOf(hand.getSumOfFaces()));
        allHeartsOnHandLabel.setText(hand.getAllHeartsOnHand());
        if(hand.queenOfSpadesPresent())
            queenOfSpadesPresentLabel.setText("Yes");
        else queenOfSpadesPresentLabel.setText("No");
        if(hand.handContainsFlush())
            handContainsFlushLabel.setText("Yes");
        else handContainsFlushLabel.setText("No");
    }
}