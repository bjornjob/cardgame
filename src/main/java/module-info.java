module edu.ntnu.idatt2001.bjornjob.oblig3.cardgame.cardgame {
    requires javafx.controls;
    requires javafx.fxml;


    opens edu.ntnu.idatt2001.bjornjob.oblig3.cardgame to javafx.fxml;
    exports edu.ntnu.idatt2001.bjornjob.oblig3.cardgame;
    exports edu.ntnu.idatt2001.bjornjob.oblig3.cardgame.cardclasses;
    opens edu.ntnu.idatt2001.bjornjob.oblig3.cardgame.cardclasses to javafx.fxml;
}