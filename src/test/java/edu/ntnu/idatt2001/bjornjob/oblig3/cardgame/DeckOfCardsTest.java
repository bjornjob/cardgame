package edu.ntnu.idatt2001.bjornjob.oblig3.cardgame;

import edu.ntnu.idatt2001.bjornjob.oblig3.cardgame.cardclasses.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author bjornjob
 * @version 1.o
 * @since 2020-03-25
 */
@DisplayName("Playing card tests")
public class DeckOfCardsTest {

    @DisplayName("Positive tests for the class ´DeckOfCards´")
    public class PositiveTests {

        @Test
        @DisplayName("dealHand-method deals the correct amount of cards")
        public void testThatDealHandMethodWorksAsIntended() {
            DeckOfCards deckOfCards = new DeckOfCards();
            ArrayList<PlayingCard> dealtCards = deckOfCards.dealHand(5);
            assertEquals(5, dealtCards.size());
        }

        @Nested
        @DisplayName("Tests for the constructor")
        public class constructorTests {

            @Test
            @DisplayName("Initializing a deck of cards using the constructor")
            public void testInitializingADeckOfCards() {
                try {
                    DeckOfCards deckOfCards = new DeckOfCards();
                    assertEquals(52, deckOfCards.getDeck().size());
                } catch (IllegalArgumentException e) {
                    fail("'testValidInputForDeckOfCardsObject' failed");
                }
            }
        }
    }

    @Nested
    @DisplayName("Negative tests for the class ´DeckOfCards´")
    public class NegativeTests {

        @Test
        @DisplayName("Dealing a hand with 0 cards")
        public void testDealingAHandWithZeroCards() {
            try {
                DeckOfCards deckOfCardsOne = new DeckOfCards();
                deckOfCardsOne.dealHand(0);
            } catch (IllegalArgumentException e) {
                assertEquals("Inputted number must be between 1 and 52", e.getMessage());
            }
        }

        @Test
        @DisplayName("Dealing a hand with 53 cards")
        public void testDealingAHandWithFiftyThreeCards() {
            try {
                DeckOfCards deckOfCardsOne = new DeckOfCards();
                deckOfCardsOne.dealHand(53);
            } catch (IllegalArgumentException e) {
                assertEquals("Inputted number must be between 1 and 52", e.getMessage());
            }
        }
    }
}