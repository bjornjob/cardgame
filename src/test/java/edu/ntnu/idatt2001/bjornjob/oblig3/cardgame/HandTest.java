package edu.ntnu.idatt2001.bjornjob.oblig3.cardgame;

import edu.ntnu.idatt2001.bjornjob.oblig3.cardgame.cardclasses.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author bjornjob
 * @version 1.o
 * @since 2020-03-25
 */
@DisplayName("Playing card tests")
public class HandTest {
    private Hand cardHandOne;
    private Hand cardHandTwo;

    @BeforeEach
    public void TestData() {
        ArrayList<PlayingCard> listOfCardsOne = new ArrayList<>();
        listOfCardsOne.add(new PlayingCard('S', 12));
        listOfCardsOne.add(new PlayingCard('H', 10));
        listOfCardsOne.add(new PlayingCard('C', 2));
        listOfCardsOne.add(new PlayingCard('D', 8));
        listOfCardsOne.add(new PlayingCard('H', 13));
        cardHandOne = new Hand(listOfCardsOne);

        ArrayList<PlayingCard> listOfCardsTwo = new ArrayList<>();
        listOfCardsTwo.add(new PlayingCard('C', 3));
        listOfCardsTwo.add(new PlayingCard('C', 2));
        listOfCardsTwo.add(new PlayingCard('C', 12));
        listOfCardsTwo.add(new PlayingCard('C', 13));
        listOfCardsTwo.add(new PlayingCard('C', 9));
        listOfCardsTwo.add(new PlayingCard('C', 10));
        cardHandTwo = new Hand(listOfCardsTwo);
    }

    @Nested
    @DisplayName("Positive tests for the class ´Hand´")
    class PositiveTests {

        @Test
        @DisplayName("The sum of the hand is as expected")
        public void testGetSumOfFacesMethod() {
            assertEquals(45, cardHandOne.getSumOfFaces());
            assertEquals(49, cardHandTwo.getSumOfFaces());

        }

        @Test
        @DisplayName("All hearts on the hand is returned correctly as a string")
        public void testGetAllHeartsOnHandMethod() {
            assertEquals("H10, H13", cardHandOne.getAllHeartsOnHand());
            assertEquals("No hearts", cardHandTwo.getAllHeartsOnHand());
        }

        @Test
        @DisplayName("The queen of spades is present")
        public void testQueenOfSpadesPresentMethodForTrue() {
            assertTrue(cardHandOne.queenOfSpadesPresent());
        }

        @Test
        @DisplayName("The queen of spades is not present")
        public void testQueenOfSpadesPresentMethodForFalse() {
            assertFalse(cardHandTwo.queenOfSpadesPresent());
        }

        @Test
        @DisplayName("The hand contains a flush")
        public void testHandContainsFlushMethod() {
            assertFalse(cardHandOne.handContainsFlush());
            assertTrue(cardHandTwo.handContainsFlush());
        }
    }
}