package edu.ntnu.idatt2001.bjornjob.oblig3.cardgame;

import edu.ntnu.idatt2001.bjornjob.oblig3.cardgame.cardclasses.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author bjornjob
 * @version 1.o
 * @since 2020-03-25
 */
@DisplayName("Playing card tests")
public class PlayingCardTest {

    @Nested
    @DisplayName("Positive tests for the class ´PlayingCard´")
    public class PositiveTests {

        @Test
        @DisplayName("getAsString-method works as intended")
        public void testGetAsStringMethod() {
            PlayingCard playingCardOne = new PlayingCard('H', 9);
            assertEquals(playingCardOne.getAsString(), "H9");
            PlayingCard playingCardTwo = new PlayingCard('s', 12);
            assertEquals(playingCardTwo.getAsString(), "S12");
        }

        @Nested
        @DisplayName("Tests for valid input in the constructor")
        public class constructorTests {

            @Test
            @DisplayName("Valid object-input in constructor")
            public void testValidInputForPlayingCard() {
                try {
                    PlayingCard playingCardOne = new PlayingCard('S', 10);
                    PlayingCard playingCardTwo = new PlayingCard('H', 11);
                    PlayingCard playingCardThree = new PlayingCard('D', 12);
                    PlayingCard playingCardFour = new PlayingCard('C', 13);
                } catch (Exception e) {
                    fail("'testValidInputForPlayingCard' failed");
                }
            }
        }
    }
}